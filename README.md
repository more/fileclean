# fileclean

Script to age and eventually delete filesystem files (logs, etc).

To be called from cron, for example:

00 04 * * * /usr/local/bin/fileclean root /tmp "*" "*.gz" 7 365 > /var/log/fileclean-smsh07 2>&1